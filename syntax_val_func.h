#ifndef SYNTAX_VAL_FUNC_H
#define SYNTAX_VAL_FUNC_H

#include <stdbool.h>

typedef enum {FALLO_EJECUCION, VALIDO, INVALIDO} val_result_t;

val_result_t syntax_val(char* expresion);

#endif // SYNTAX_VAL_FUNC_H