#include "cola.h"
#include <stdlib.h>

typedef struct nodo {
    void* dato;
    struct nodo* siguiente;
} nodo_t;

struct cola {
    nodo_t* primero;
    nodo_t* ultimo;
};


/* *****************************************************************
 *                    FUNCIONES AUXILIARES
 * *****************************************************************/
nodo_t* _nodo_crear(void* dato){
    nodo_t * nodo = malloc(sizeof(nodo_t));
    if(nodo == NULL){
        return false;
    }

    nodo->dato = dato;
    nodo->siguiente = NULL;

    return nodo;
}
/* *****************************************************************
 *                    PRIMITIVAS DE LA COLA
 * *****************************************************************/
cola_t* cola_crear(){
    cola_t* cola = malloc(sizeof(cola_t));
    if(cola == NULL){
        return NULL;
    }

    cola->primero = NULL;
    cola->ultimo = NULL;
    return cola;
}

void cola_destruir(cola_t *cola, void destruir_dato(void*)){
    while(!cola_esta_vacia(cola)){
        void* dato = cola_desencolar(cola);
        if(destruir_dato != NULL){
            destruir_dato(dato);
        }
    }
    free(cola);
}

bool cola_esta_vacia(const cola_t *cola){
   return cola->primero == NULL;
}

bool cola_encolar(cola_t *cola, void* valor){
    nodo_t* nuevo_nodo = _nodo_crear(valor);
    if(!nuevo_nodo){
        return false;
    }

    if(cola_esta_vacia(cola)){
        cola->primero = cola->ultimo = nuevo_nodo;
    }
    else{
        cola->ultimo->siguiente = nuevo_nodo;
        cola->ultimo = nuevo_nodo;
    }
    return true;
}

void* cola_ver_primero(const cola_t *cola){
    return cola->primero ? cola->primero->dato: NULL;
}

void* cola_desencolar(cola_t *cola){
    if(cola_esta_vacia(cola)){
        return NULL;
    }

    nodo_t* nodo_desencolado = cola->primero;
    void* dato_desencolado = nodo_desencolado->dato;
    cola->primero = cola->primero->siguiente;
    free(nodo_desencolado);

    if(cola->primero == NULL){
        cola->ultimo = NULL;
    }

    return dato_desencolado;
}