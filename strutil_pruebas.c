#include "strutil.h"
#include <stdbool.h>
#include <string.h>
#include "testing.h"
#include <stdio.h>
#include <stdlib.h>

/* ******************************************************************
 *                   Funciones auxiliares
 * *****************************************************************/
static size_t strv_len(char** strv){
    if(!strv){
        return 0;
    }

    size_t i = 0;
    while(*strv){
        strv++;
        i++;
    }
    return i;
}

static bool strv_are_equal(char** strv1, char** strv2){
    if(strv1 == NULL && strv2 == NULL){
        return true;
    }

    size_t len1 = strv_len(strv1);
    if(len1 !=  strv_len(strv2)){
        return false;
    }

    for(size_t i = 0; i < len1 + 1; i++){
        if(strcmp(*strv1, *strv2) != 0){
            return false;
        }
    }
    return true;
}

static void strv_print(char** strv){
    if(!strv){
        printf("NULL");
        return;
    }

    printf("[");
    while(*strv){
        printf("\"%s\", ", *strv);
        strv++;
    }
    printf("NULL");
    printf("]");
}

/* ******************************************************************
 *                   split: pruebas unitarias
 * *****************************************************************/
static void split_test(char *str, char sep, char** esperado){
    char** obtenido = split(str, sep);
    bool ok = strv_are_equal(esperado, obtenido);

    printf("Prueba split(\"%s\", '%c') == ", str, sep); strv_print(esperado);
    print_test("", ok);
    if(!ok){
        printf("    Esperado: "); strv_print(esperado); printf("\n");
        printf("    Obtenido: "); strv_print(obtenido); printf("\n");
    }
    free_strv(obtenido);
}

void pruebas_alumno_split(){
    split_test("cero,uno,dos,tres", '\0', NULL);
    split_test("cero,uno,dos,tres", ',', (char *[]) {"cero", "uno", "dos", "tres", NULL});
    split_test("", ',', (char *[]) {"", NULL});
    split_test(",", ',', (char *[]) {"", "", NULL});
    split_test(",uno,dos,tres", ',', (char *[]) {"", "uno", "dos", "tres", NULL});
    split_test("cero,uno,dos,", ',', (char *[]) {"cero", "uno", "dos", "", NULL});
    split_test(",uno,dos,tres,", ',', (char *[]) {"", "uno", "dos", "tres", "", NULL});
    split_test(",uno,,tres,cuatro", ',', (char *[]) {"", "uno", "", "tres", "cuatro", NULL});
    split_test("cero,uno,dos,tres", '#', (char *[]) {"cero,uno,dos,tres", NULL});
    split_test("cero#uno#dos#tres", '#', (char *[]) {"cero", "uno", "dos", "tres", NULL});
}

/* ******************************************************************
 *                   join: pruebas unitarias
 * *****************************************************************/
static void test_join(char** strv, char sep, char* esperado){
    char* obtenido = join(strv, sep);
    bool ok = strcmp(esperado, obtenido) == 0;

    printf("Prueba join("); strv_print(strv); printf(", '%c') == \"%s\"", sep, esperado);
    print_test("", ok);

    if(!ok){
        printf("Esperado: %s", esperado);
        printf("Obtnido: %s", obtenido);
    }
    free(obtenido);
}

void pruebas_alumno_join(){
    char** strv1 = (char* []){"cero", "uno", "dos", "tres", NULL};
    test_join(strv1, ';', "cero;uno;dos;tres");
    print_test("Prueba strv nose modificó", strv_are_equal(strv1, (char* []){"cero", "uno", "dos", "tres", NULL}));

    char** strv2 = NULL;
    test_join(strv2, ';', "");
    print_test("Prueba strv nose modificó", strv_are_equal(strv2, NULL));

    char** strv3 = (char* []){"", NULL};
    test_join(strv3, ';', "");
    print_test("Prueba strv nose modificó", strv_are_equal(strv3, (char* []){"", NULL}));

    char** strv4 = (char* []){"abc" , NULL};
    test_join(strv4, ',', "abc");
    print_test("Prueba strv nose modificó", strv_are_equal(strv4, (char* []){"abc", NULL}));

    char** strv5 = (char* []){"", "", NULL};
    test_join(strv5, ',', ",");
    print_test("Prueba strv nose modificó", strv_are_equal(strv5, (char* []){"", "", NULL}));

    char** strv6 = (char* []){"\0" , NULL};
    test_join(strv6, ',', "");
    print_test("Prueba strv nose modificó", strv_are_equal(strv6, (char* []){"\0" , NULL}));

    char** strv7 = NULL;
    test_join(strv7, ',', "");
    print_test("Prueba strv nose modificó", strv_are_equal(strv7, NULL));
}

/* ******************************************************************
 *                             main
 * *****************************************************************/
 int main(){
    printf("PRUEBAS DEL ALUMNO: split, join, free_strv\n\n");
    pruebas_alumno_split(); printf("\n");
    pruebas_alumno_join(); printf("\n");

    printf("Errores: %i\n", failure_count());
    return failure_count() > 0;
 }