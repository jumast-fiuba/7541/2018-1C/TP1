#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

/* ******************************************************************
 *                            auxiliares
 * *****************************************************************/
typedef struct file_reader{
    FILE* archivo;
    char* linea;
    size_t cant;
    bool eof;
    bool error;
} file_reader_t;

static file_reader_t* file_reader_crear(char* nombre_de_archivo){
    FILE* archivo = fopen(nombre_de_archivo, "r");
    if(archivo == NULL){
        fprintf(stderr, "Archivo erroneo\n");
        return NULL;
    }

    file_reader_t* file_reader = malloc(sizeof(file_reader_t));
    if(file_reader == NULL){
        fclose(archivo);
        return NULL;
    }

    file_reader->archivo = archivo;
    file_reader->linea = NULL;
    file_reader->cant = 0;
    file_reader->eof = false;
    file_reader->error = false;

    return file_reader;
}

static void file_reader_destruir(file_reader_t* file_reader){
    fclose(file_reader->archivo);
    free(file_reader->linea);
    free(file_reader);
}

static void file_reader_leer(file_reader_t* file_reader){

     if(file_reader->eof){
         return;
     }

     size_t leidos = getline(&file_reader->linea, &file_reader->cant, file_reader->archivo);
     if(leidos == -1){
         bool eof = feof(file_reader->archivo) != 0 ? true :false;
         if(eof){
             file_reader->eof = true;
             return;
         }
         else{
             file_reader->error = true;
             return;
         }
     }
     return;
}

/* ******************************************************************
 *                              main
 * *****************************************************************/
 int main(int argc, char** argv){
    if(argc != 3){
        fprintf(stderr, "Cantidad de parametros erronea\n");
        return EXIT_FAILURE;
    }

    file_reader_t* file_reader_1 = file_reader_crear(argv[1]);
    if(file_reader_1 == NULL)
        return EXIT_FAILURE;

    file_reader_t* file_reader_2 = file_reader_crear(argv[2]);
    if(file_reader_2 == NULL){
        file_reader_destruir(file_reader_1);
        return EXIT_FAILURE;
    }

    size_t cant_lineas = 1;
    char* linea_vacia = "\n";

    while(!file_reader_1->eof || !file_reader_2->eof){

        file_reader_leer(file_reader_1);
        file_reader_leer(file_reader_2);

        if(file_reader_1->error || file_reader_2->error){
            file_reader_destruir(file_reader_1);
            file_reader_destruir(file_reader_2);
            return EXIT_FAILURE;
        }

        if(file_reader_1->eof && file_reader_2->eof){
            file_reader_destruir(file_reader_1);
            file_reader_destruir(file_reader_2);
            return EXIT_SUCCESS;
        }

        if(strcmp(file_reader_1->linea, file_reader_2->linea) != 0){
            printf("Diferencia en linea %zu\n", cant_lineas);
            printf("< %s", !file_reader_1->eof ? file_reader_1->linea : linea_vacia);
            printf("---\n");
            printf("> %s", !file_reader_2->eof ? file_reader_2->linea : linea_vacia);
        }
        cant_lineas++;
    }
}