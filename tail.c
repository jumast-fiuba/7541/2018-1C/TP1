#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cola.h"

/* ******************************************************************
 *                   Funciones auxiliares
 * *****************************************************************/
static size_t* str_to_sizet(char* string){
    char* endptr;
    size_t convertido = strtoul(string, &endptr, 10);

    if(*endptr != '\0'){
        return NULL;
    }

    size_t* resultado = malloc(sizeof(size_t));
    if(resultado == NULL){
        return NULL;
    }
    *resultado = convertido;
    return resultado;
}

static char* str_copiar(const char* str){
    char* copia = malloc(1 + strlen(str));
    if(!copia){
        return NULL;
    }

    strcpy(copia, str);
    return copia;
}

static cola_t* obtener_ultimas_n_lineas(FILE* file, size_t n){
    cola_t* cola = cola_crear();
    if(cola == NULL)
        return NULL;

    size_t cant_cola = 0;
    char* linea = NULL;
    size_t cant = 0;

    while(true){
        getline(&linea, &cant, file);

        if(feof(file)){
            break;
        }

        if(ferror(file)){
            cola_destruir(cola, free);
            free(linea);
            return NULL;
        }

        char* linea_copia = str_copiar(linea);
        if(linea_copia == NULL){
            cola_destruir(cola, free);
            free(linea);
            return NULL;
        }

        bool encolar_ok = cola_encolar(cola, linea_copia);
        if(encolar_ok == false){
            cola_destruir(cola, free);
            free(linea);
            free(linea_copia);
            return NULL;
        }

        cant_cola++;
        if(cant_cola > n){
            free(cola_desencolar(cola));
            cant_cola--;
        }
    }

    free(linea);
    return cola;
}

static void imprimir_lineas(cola_t* cola){
    while(!cola_esta_vacia(cola)){
        char* linea = cola_desencolar(cola);
        printf("%s", linea);
        free(linea);
    } 
}
/* ******************************************************************
 *                              main
 * *****************************************************************/
int main(int argc, char** argv){
    if(argc != 2){
        fprintf(stderr, "Cantidad de parametros erronea\n");
        return EXIT_FAILURE;
    }

    size_t* n = str_to_sizet(argv[1]);
    if(n == NULL){
        fprintf(stderr, "Tipo de parametro incorrecto\n");
        return EXIT_FAILURE;
    }

    cola_t* cola = obtener_ultimas_n_lineas(stdin, *n);
    if(cola == NULL){
        free(n);
        fprintf(stderr, "Error no especificado\n");
        return EXIT_FAILURE;
    }

    imprimir_lineas(cola);
    free(n);
    cola_destruir(cola, NULL);

    return EXIT_SUCCESS;
}