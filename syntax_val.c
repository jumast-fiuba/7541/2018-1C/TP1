#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "syntax_val_func.h"

/* ******************************************************************
 *                              main
 * *****************************************************************/
int main(){
    char* linea = NULL;
    size_t cant = 0;
    while((getline(&linea, &cant, stdin)) > 0){

        val_result_t val_result = syntax_val(linea);
        if(val_result == FALLO_EJECUCION){
            free(linea);
            fprintf(stdout, "%s\n", "Error en la ejecución de la validación");
            return EXIT_FAILURE;
        }

        fprintf(stdout, "%s\n", syntax_val(linea) == VALIDO ? "OK" : "ERROR");
    }
    free(linea);
    return EXIT_SUCCESS;
}