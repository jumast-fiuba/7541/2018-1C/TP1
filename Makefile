CC = gcc
CFLAGS = -g -std=c99 -Wall -Wtype-limits -pedantic -Wconversion -Wno-sign-conversion -Werror 
VALFLAGS = --leak-check=full --track-origins=yes --show-reachable=yes
ZIPNAME = tp1.zip

all: build_strutil_pruebas build_tail build_diff build_syntax build_syntax_pruebas
valgrind_all: valgrind_strutil_pruebas valgrind_syntax_pruebas

TESTING_FILES = testing.c testing.h
STRUTIL_FILES = strutil.c strutil.h
COLA_FILES = cola.c cola.h
PILA_FILES = pila.c pila.h

### strutil ###
STRUTIL_PRUEBAS_FILES = strutil_pruebas.c $(TESTING_FILES) $(STRUTIL_FILES)
STRUTIL_PRUEBAS_EXEC = strutil_pruebas

build_strutil_pruebas: $(STRUTIL_PRUEBAS_FILES)
	$(CC) $(CFLAGS) -o $(STRUTIL_PRUEBAS_EXEC) $(STRUTIL_PRUEBAS_FILES)

valgrind_strutil_pruebas: build_strutil_pruebas
	valgrind $(VALFLAGS) ./strutil_pruebas

### tail ###
N = 0
USE_FILE = false
TAIL_FILES = tail.c $(COLA_FILES)
TAIL_EXEC = tail

build_tail: $(TAIL_FILES)
	$(CC) $(CFLAGS) -o $(TAIL_EXEC) $(TAIL_FILES)

valgrind_tail: build_tail
 ifeq ($(USE_FILE), false)
		valgrind $(VALFLAGS) ./$(TAIL_EXEC) $(N)
 else
		cat "tail_archivos/nombres.txt"
		cat "tail_archivos/nombres.txt" | valgrind $(VALFLAGS) ./$(TAIL_EXEC) $(N)
 endif

### diff ###
A1 = "diff_archivos/nombres1.txt"
A2 = "diff_archivos/nombres2.txt"
A3 = "diff_archivos/nombres3.txt"
DIFF_FILES = diff.c
DIFF_EXEC = diff

build_diff: $(DIFF_FILES)
	$(CC) $(CFLAGS) -o $(DIFF_EXEC) $(DIFF_FILES)

diff_11: build_diff
	./$(DIFF_EXEC) $(A1) $(A1)

diff_12: build_diff
	./$(DIFF_EXEC) $(A1) $(A2)

diff_21: build_diff
	./$(DIFF_EXEC) $(A2) $(A1)

diff_13: build_diff
	./$(DIFF_EXEC) $(A1) $(A3)

diff_31: build_diff
	./$(DIFF_EXEC) $(A3) $(A1)

diff_v11: build_diff
	valgrind $(VALFLAGS) ./$(DIFF_EXEC) $(A1) $(A1)

diff_v12: build_diff
	valgrind $(VALFLAGS) ./$(DIFF_EXEC) $(A1) $(A2)

diff_v21: build_diff
	valgrind $(VALFLAGS) ./$(DIFF_EXEC) $(A2) $(A1)

diff_v13: build_diff
	valgrind $(VALFLAGS) ./$(DIFF_EXEC) $(A1) $(A3)

diff_v31: build_diff
	valgrind $(VALFLAGS) ./$(DIFF_EXEC) $(A3) $(A1)

### syntax_val ###
N = 0
SYNTAX_FILES = syntax_val_func.c syntax_val_func.h syntax_val.c
SYNTAX_FILES += $(PILA_FILES)
SYNTAX_EXEC = syntax_val

build_syntax: $(SYNTAX_FILES)
	$(CC) $(CFLAGS) -o $(SYNTAX_EXEC) $(SYNTAX_FILES)

valgrind_syntax: build_syntax
 ifeq ($(N), 0)
		valgrind $(VALFLAGS) ./$(SYNTAX_EXEC)
 else
		cat "syntax_val_archivos/scripts$(N).txt"
		cat "syntax_val_archivos/scripts$(N).txt" | valgrind ./$(SYNTAX_EXEC)
 endif

SYNTAX_PRUEBAS_FILES = syntax_val_tests.c syntax_val_func.c syntax_val_func.h
SYNTAX_PRUEBAS_FILES += $(PILA_FILES)
SYNTAX_PRUEBAS_FILES += $(TESTING_FILES)
SYNTAX_PRUEBAS_EXEC = syntax_pruebas
build_syntax_pruebas: $(SYNTAX_PRUEBAS_FILES) 
	$(CC) $(CFLAGS) -o $(SYNTAX_PRUEBAS_EXEC) $(SYNTAX_PRUEBAS_FILES)

valgrind_syntax_pruebas: build_syntax_pruebas
	valgrind $(VALFLAGS) ./$(SYNTAX_PRUEBAS_EXEC)

### etc ###
clean:
	rm -f *.o $(STRUTIL_PRUEBAS_EXEC) $(TAIL_EXEC) $(DIFF_EXEC) $(SYNTAX_EXEC) $(SYNTAX_PRUEBAS_EXEC) $(ZIPNAME)

zip:
	zip $(ZIPNAME) strutil.c cola.c cola.h pila.c pila.h tail.c diff.c syntax_val.c syntax_val_func.c syntax_val_func.h deps.mk



