#include "strutil.h"
#include <stdbool.h>
#include <string.h>
#include "testing.h"
#include <stdio.h>
#include <stdlib.h>
#include "syntax_val_func.h"

/* ******************************************************************
 *                 syntax_val: pruebas unitarias
 * *****************************************************************/
static void probar_expresion(char* expresion, bool esperado){
    val_result_t val_result = syntax_val(expresion);
    bool obtenido = val_result == VALIDO ? true : false;

    printf("Prueba syntax_val(\"%s\") == %s", expresion, esperado ? "OK" : "ERROR");
    print_test("", obtenido == esperado);
}

void syntax_val_pruebas_alumno(){
    probar_expresion("[]", true);
    probar_expresion("[()]", true);
    probar_expresion("[(){}]", true);
    probar_expresion("[[[[[]]]]]", true);
    probar_expresion("[[(({}))]]", true);
    probar_expresion("[hola mundo]", true);

    probar_expresion("[]()", true);
    probar_expresion("[(])",false);
    probar_expresion("()(", false);
    probar_expresion("[(}]", false);

    probar_expresion("['hola']", true);
    probar_expresion("('mundo')", true);
    probar_expresion("[']']", true);
    probar_expresion("[]'hola'", true);
    probar_expresion("[']", false);
    probar_expresion("[]'", false);

    probar_expresion("[abc()]", true);
    probar_expresion("[()abc]", true);
    probar_expresion("abc[]", true);
    probar_expresion("[]abc", true);

    probar_expresion("", true);
    probar_expresion("notieneparentesis", true);
    probar_expresion("\'(((\'", true);
}

int main(){
    printf("PRUEBAS DEL ALUMNO: syntax_val\n");
    syntax_val_pruebas_alumno();
    printf("Errores: %i\n", failure_count());
    return failure_count() > 0;
 }