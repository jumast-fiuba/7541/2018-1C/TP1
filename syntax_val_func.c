#include <stdlib.h>
#include <stdbool.h>
#include "pila.h"
#include "syntax_val_func.h"

/* ******************************************************************
 *                     funciones auxiliares
 * *****************************************************************/
bool son_correspondientes(char izquierdo, char derecho){
    if (izquierdo == '(' && derecho == ')') return true;
    if (izquierdo == '[' && derecho == ']') return true;
    if (izquierdo == '{' && derecho == '}') return true;
      if (izquierdo == '\'' && derecho == '\'') return true;
    return false;
}

/* ******************************************************************
 *                           syntax_val
 * *****************************************************************/
val_result_t syntax_val(char* expresion){

    pila_t* pila = pila_crear();
    if(pila == NULL){
        return FALLO_EJECUCION;
    }

    bool syntax_ok = true;
    bool comilla_apilada = false;

    while(*expresion != '\0'){
        char c = *expresion;

        bool apilar_izquierdo = (comilla_apilada == false && (c == '(' || c == '[' || c == '{'));
        bool apilar_comilla = (comilla_apilada == false && (c == '\''));
        bool desapilar_derecho = (comilla_apilada == false && (c == ')' || c == ']' || c == '}'));
        bool desapilar_comilla = (comilla_apilada == true && (c == '\''));

        if(apilar_izquierdo || apilar_comilla){
            bool apilar_ok = pila_apilar(pila, expresion);
            if(apilar_ok == false){
                pila_destruir(pila);
                return FALLO_EJECUCION;
            }
        }
        else if(desapilar_derecho || desapilar_comilla){
            char* desapilado = pila_desapilar(pila);
            if(desapilado == NULL){
                syntax_ok = false;
                break;
            }

            char c2 = *desapilado;
            if(!son_correspondientes(c2, c)){
                syntax_ok = false;
                break;
            }
        }
        if(apilar_comilla){
            comilla_apilada = true;
        }
        else if(desapilar_comilla){
            comilla_apilada = false;
        }
        expresion++;
    }

    bool pila_vacia = pila_esta_vacia(pila);
    pila_destruir(pila);
    return (syntax_ok && pila_vacia) ? VALIDO : INVALIDO;
}
