#include <stdlib.h>
#include <string.h>
#include "strutil.h"

size_t strvlen(char** strv){
    if(strv == NULL || *strv == NULL){
        return 1;
    }

    size_t len = 0;
    while(*strv != NULL){
        len+=1;
        len+=strlen(*strv);
        strv++;
    }
    return len;
}

// Calcula la cantidad de ocurrencias del caracter 'sep' en la cadena str.
size_t count(const char* str, char sep){
    size_t sepcount = 0;
    size_t len = strlen(str);
    for(size_t i = 0; i <= len; i++){
        char valor = str[i];
        if(valor == sep){
            sepcount++;
        }
    }
    return sepcount;
}

char** split(const char* str, char sep){
        if(sep == '\0'){
            return NULL;
        }

        size_t sepcount = count(str, sep);

        char** splited = malloc((sepcount + 2) * sizeof(char*));
        if(splited == NULL){
            return NULL;
        }


        size_t len = strlen(str);
        size_t substrcount = 0;
        size_t posini = 0;
        size_t posfin = 0;
        for(size_t i = 0; i <= len; i++){
            char valor = str[i];
            if(valor == sep || valor == '\0'){
                size_t sublen = posfin - posini;
                char* substr = malloc((sublen + 1) * sizeof(char*));
                if(substr == NULL){
                    splited[substrcount] = NULL;
                    free_strv(splited);
                    return NULL;
                }

                substr = strncpy(substr, &str[posini], sublen);
                substr[sublen] = '\0'; //innecesario
                splited[substrcount] = substr;
                substrcount++;

                posini = posfin + 1;
                posfin = posini;
            }
            else{
                posfin++;
            }
        }

        splited[substrcount] = NULL;
        return splited;
    }

char* join(char** strv, char sep){
    size_t len = strvlen(strv);
    char* joined = malloc(sizeof(char) * len);
    if(joined == NULL){
        return NULL;
    }

    if(strv == NULL){
        joined[len - 1] = '\0';
        return joined;
    }

    char* joined_ptr = joined;
    while(*strv != NULL){
        char* str_ptr = *strv;
        while(*str_ptr != '\0'){
            *joined = *str_ptr;
            str_ptr++;
            joined++;
        }
        *joined = sep;
        joined++;
        strv++;
    }
    joined_ptr[len - 1] = '\0';
    return joined_ptr;
}

void free_strv(char* strv[]){
    if(strv == NULL){
        return;
    }

    char** strv_ptr = strv;
    while(*strv_ptr != NULL){
        free(*strv_ptr);
        strv_ptr++;
    }
    free(strv);
}